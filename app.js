const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser');
const User = require('./models/user');

const app = express();

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
 
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
 
app.get('/', function (req, res) {
    res.render('home');
});
app.get('/add', function (req, res) {
    res.render('add');
});
app.post('/add', urlencodedParser, function (req, res) {
    User.create({
        nome: req.body.nome,
        email: req.body.email,
        idade: req.body.idade
    }).then(()=> {
        User.findAll({
            order: [
                // Will escape title and validate DESC against a list of valid direction parameters
                ['nome', 'ASC']]
        }).then( users => {
            res.render('list', {message: 'O usuário foi cadastrado com sucesso !', users: users});
        });
    });
});
app.get('/list', function (req, res) {
    User.findAll({
        order: [
            // Will escape title and validate DESC against a list of valid direction parameters
            ['nome', 'ASC']]
    }).then( users => {
        res.render('list', {users: users});
    });  
});
app.get('/del/:id', function (req, res) {
    // res.send(req.params.uid);
    User.findAll({
        where: {
          id: req.params.id
        }
    }).then( users => {
        res.render('del', {user: users});
    });
});
app.get('/destroy/:id', function (req, res) {
    // res.send(req.params.uid);
    User.destroy({
        where: {
          id: req.params.id
        }
    }).then( () => {
        res.redirect('../list');
    });
});
app.listen(3000);