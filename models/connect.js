const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('cadastro', 'root', '', {
  host: 'localhost',
  dialect: 'mysql'
});

module.exports = {
    Sequelize: Sequelize,
    sequelize: sequelize
}