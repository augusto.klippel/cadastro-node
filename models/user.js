const conn = require('./connect');

const User = conn.sequelize.define('users', {
    // attributes
    nome: {
      type: conn.Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: conn.Sequelize.STRING
      // allowNull defaults to true
    },
    idade: {
      type: conn.Sequelize.INTEGER
      // allowNull defaults to true
    }
  }, {
    // options
  });

//   User.sync({ force: true });

module.exports = User;